-- Build script for expkv
module     = "expkv-bundle"

pkgversions = {
  expkv = "2.1",
  cs    = "1.4",
  def   = "1.1",
  opt   = "1.1",
  pop   = "1.0",
}
pkgdates = {
  expkv = "2024-12-26",
  cs    = "2024-12-16",
  def   = "2024-12-16",
  opt   = "2024-12-16",
  pop   = "2023-01-23",
}

-- get the latest package's date
bundledate = nil
tagyear = nil
do
  local by = 0
  local bm = 0
  local bd = 0
  for _,val in pairs(pkgdates) do
    local y, m, d = string.match(val, "(%d+)%-(%d+)%-(%d+)")
    if by < tonumber(y) then
      by = tonumber(y)
      bm = tonumber(m)
      bd = tonumber(d)
    elseif bm < tonumber(m) then
      bm = tonumber(m)
      bd = tonumber(d)
    elseif bd < tonumber(d) then
      bd = tonumber(d)
    end
  end
  tagyear = string.format("%04d", by)
  bundledate = tagyear .. string.format("-%02d-%02d", bm, bd)
end

-- update package date and version
tagfiles = {
  "*.dtx",
  "expkv-bundle.tex",
  "expkv-bundle.ins",
  "README.md",
  "CTAN.md",
}
function update_tag(file, content, tagname, tagdate)
  -- update copyright notices
  content = string.gsub(content,
    "(Copyright%s*%(C%)%s*%d%d%d%d%-)%d%d%d%d",
    "%1" .. tagyear)
  if string.match(file, "%.md") then
    return string.gsub(content, "%d%d%d%d%-%d%d%-%d%d", bundledate)
  elseif string.match(file, "expkv%-bundle.tex") then
    return string.gsub(content,
      "\\date{%d%d%d%d%-%d%d%-%d%d}",
      "\\date{" .. bundledate .. "}")
  end
  local matched = false
  if tagname == nil then
    for name, date in pairs(pkgdates) do
      if string.match(file, name .. ".dtx") then
        matched = true
        tagdate = date
        tagname = pkgversions[name]
        break
      end
    end
  end
  if matched then
    content = string.gsub(content,
      "\\newcommand%*\\ekv(%w?)Date{%d%d%d%d%-%d%d%-%d%d}",
      "\\newcommand*\\ekv%1Date{" .. tagdate .. "}")
    content = string.gsub(content,
      "\\newcommand%*\\ekv(%w?)Version{%d%.%d%w?}",
      "\\newcommand*\\ekv%1Version{" .. tagname .. "}")
    content = string.gsub(content,
      "\\def\\ekv(%w?)Date{%d%d%d%d%-%d%d%-%d%d}",
      "\\def\\ekv%1Date{" .. tagdate .. "}")
    return string.gsub(content,
      "\\def\\ekv(%w?)Version{%d%.%d%w?}",
      "\\def\\ekv%1Version{" .. tagname .. "}")
  end
  return content
end

-- test with pdfTeX and the LaTeX format
checkengines = {"pdftex","luatex"}
checkformat  = "latex"

-- from which files to build
sourcefiles = {"*.dtx", "*.ins"}
unpackfiles = {"expkv-bundle.ins", "expkv-opt-2020-10-10.dtx"}

-- which files to put in the tds
installfiles = {
  "expkv.sty", "expkv.tex", "t-expkv.tex",
  "expkv-*.sty", "expkv-*.tex", "t-expkv-*.tex",
}
textfiles    = {"README.md", "CTAN.md"}

-- how the documentation is build
docfiledir = "./Doc"
typesetfiles = {"expkv-bundle.tex"}
docfiles = {"*.tex"}
typesetruns  = 4

-- make sure that expkv.tex ends up in the generic path
packtdszip   = true
tdslocations = {
  "tex/generic/expkv-bundle/expkv.tex",
  "tex/generic/expkv-bundle/expkv-cs.tex",
  "tex/generic/expkv-bundle/expkv-def.tex",
  "tex/generic/expkv-bundle/expkv-pop.tex",
  "tex/context/third/expkv-bundle/t-expkv.tex",
  "tex/context/third/expkv-bundle/t-expkv-cs.tex",
  "tex/context/third/expkv-bundle/t-expkv-def.tex",
  "tex/context/third/expkv-bundle/t-expkv-pop.tex",
}

-- CTAN upload
ctanreadme    = "CTAN.md"
uploadconfig  = {
  pkg         = module,
  author      = "Jonathan P. Spratte",
  version     = bundledate,
  license     = "lppl1.3c",
  summary     = "An expandable key=val implementation and friends",
  topic       = "keyval",
  ctanPath    = "/macros/generic/expkv-bundle",
  repository  = "https://gitlab.com/islandoftex/texmf/expkv-bundle",
  bugtracker  = "https://gitlab.com/islandoftex/texmf/expkv-bundle/-/issues",
  update      = true,
  description = [[
`expkv-bundle` is a collection of different packages that provide key=value
functionality in plainTeX, LaTeX, and ConTeXt.

At the core the `expkv` package implements two expandable key=value parsers that
are somewhat fast and robust against common bugs in many key=value
implementations (no accidental brace stripping, no fragility for active commas
or equals signs).

`expkv-cs` enables users to define expandable key=value macros in a comfortable
and straight forward way.

`expkv-def` provides an interface to define common key types for `expkv` similar
to the key defining interfaces of wide spread key=value implementations.

`expkv-opt` allows to parse package or class options in LaTeX via `expkv`.

`expkv-pop` is a utility package to define prefix oriented parsers that allow a
somewhat natural formulation (it provides the core functionality for the
key-defining front ends of both `expkv-cs` and `expkv-def`).
  ]]
}
