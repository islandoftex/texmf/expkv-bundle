# TODO

## `expkv-def`
- fix loading the package in plain (sometime in the past a `\newtoks` sneaked
    into the argument of a macro).

# 2024-12-26

## `expkv`
- `\ekvsetdefSneaked` and `\ekvsetSneakeddef` no longer define the macro as
    `\long` automatically (use a prefix if you need to)
- fix `\ekvsetdefSneaked` not being long itself (to allow explicit `\par` tokens
    inside the pre-sneaked material)

## `expkv-cs`
- improve the `choice` keys to allow arbitrary forwarded values (might break
    existing choices containing an equals sign)
- fix documentation of `enum` type

## `expkv-def`
- add the `alias` type to copy existing keys (in `expkv-cs` it was already
    defined, not having it in `expkv-def` was only causing confusion)
- add the `choice-aliases` type to copy existing choices for the same key

## `expkv-opt`
- fix `\ekvoUseUnknownHandlers*` causing undefined behaviour for unknown
    `NoVal`-keys without a handler being defined in the set
- fix `\ekvoUseUnknownHandlers` removing handling of defined options in
    `\ekvoProcessLocalOptions` for class files
- remove deprecated `\ekvoProcessUnusedGlobalOptions`

# 2023-01-10

## `expkv`
- add an expansion control mechanism to key=value input (`exp:`notation)
- allow access to the raw key names in unknown handlers (might break existing
    code if `\par` is part of a key name and your unknown handlers aren't
    defined `\long`, also might break existing uses of `\ekvletunknown` and
    `\ekvletunknownNoVal`, as the macros should now expect an argument more)
- `\ekvsetdef` no longer defines the macro as `\long` automatically (use a
    prefix if you need to)
- add `\ekvmorekv`
- add `\ekvcompile`
- add the `all` option to the LaTeX package loading all other packages as well
- performance tweaks

## `expkv-cs`
- the unknown handlers defined with `...` forward the raw key names now.
- the unknown handlers defined with `...` forward the key name surrounded by one
    set of braces and spaces now
- some error messages might look different now because of the usage of
    `expkv-pop`
- add secondary types `e-aggregate` and `choice`
- use `\ekvmorekv` for `meta` and `nmeta` instead of a nested `\ekvset`

## `expkv-def`
- don't parse for `new`, `also`, or `long` in choices
- add `unprotected` prefix for choices
- some error messages might look different now because of the usage of
    `expkv-pop`
- no longer document the `qdefault` type
- use `\expanded` in `estore`, `xstore`, `edata`, `xdata`, `edataT`, `xdataT`,
    `einitial`, and `edefault` instead of `\edef` expansion. Might break
    existing usage using `#`-doubling.
- use `\ekvmorekv` for `meta` and `nmeta` instead of a nested `\ekvset` (might
    affect behaviour if a set-changing key is used in the meta list, you can get
    the old behaviour using `smeta` or `snmeta`)
- also define the initial behaviour for control sequence setting types if the
    control sequence has the `\meaning` of `\relax`
- allow values stored in the underlying control sequence differing from their
    choice in `choice-store` (might break existing choices containing an equals
    sign)

## `expkv-opt`
- the deprecated `\ekvoProcessUnusedGlobalOptions` now throws an error (will be
    removed next version)
- only add new elements to the list of unused global options (avoid duplicates)
- also remove from unused global options if in a class the local options are
    identical to the global options and the unused global options aren't empty
- `\ekvoProcessFutureOptions` added
- Built in error messages now throw the errors in the name of `\@currname`

## `expkv-pop`
added
