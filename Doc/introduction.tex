\addsec{Introduction}
%The name \expkv\ stands for \emph{exp}andable \emph{k}ey-\emph{v}alue and shall
%be pronounced \enquote{axe(b) kay vee}.

This bundle consists of different packages the base being \expkv. Most of these
packages are available for \hologo{plainTeX}, \LaTeXe, and \hologo{ConTeXt}. For
stylistic reasons the package names are printed as
\expkvLogoFormat{\textit{$\langle$pkg$\rangle$}}, but the files are named
\texttt{expkv-\meta{pkg}} (CTAN-rules don't allow \string| in names), so in
order to load \expkvc\ in \LaTeXe\ you should use
\begin{enverb}[no-tcb]
  \usepackage{expkv-cs}
\end{enverb}
Each section describing a package of this bundle has next to its heading the
formats in which they work printed flush right. If more than a single format is
supported by a package the functionality is defined by the \hologo{plainTeX}
variant and the other variants only load the generic code in a way suitable for
the format.

\addssec{Terminology}
This documentation uses a few terms which always mean specific things:
\begin{description}
  \item[\kv\ pair]
    is one element in a comma separated list which contains at least one equals
    sign \emph{not} contained in any braces, and the first such equals sign is
    the separator between the \key\ (with an optional \expansion\ prefix) and
    the \val.
  \item[\key]
    means the entire left-hand side of a \kv\ pair with an optional \expansion\
    prefix stripped, or if =\val\ is omitted the complete list element, again
    with an \expansion\ prefix stripped.
  \item[\key-name]
    synonymous to \key.
  \item[\Vkey]
    describes a \key\ in a \kv\ pair.
  \item[\Nkey]
    describes a \key\ for which =\val\ was or should be omitted.
  \item[\val]
    is the right-hand side of a \kv\ pair.
  \item[\kv\ list]
    is a comma separated list containing \kv\ pairs and \Nkey{}s (each possibly
    with an \expansion\ prefix).
  \item[\kvarg]
    an argument that should get a \kv\ list.
  \item[\expansion\ prefix]
    an optional prefix in front of \key\ to specify \expansion-rules (see
    \autoref{sec:expkv:expansion}), that prefix consists of the \expansion-rules
    followed by a colon immediately followed by a space.
  \item[\expansion]
    a list of tokens specifying expansion steps for \key\ and \val.
  \item[\expansion-rule]
    a single expansion step in the \expansion-rules.
  \item[\expansion-rules]
    synonymous to \expansion.
  \item[\expnotation]
    the notation of \expansion-rules in form of the \expansion\ prefix.
  \item[key-code]
    the code that is executed for a given \key.
  \item[key-macro]
    the internal macro that stores the key-code.
\end{description}

Though not really terminology but more typographic representation I want to
highlight a distinction between two different types of code listings in this
documentation. I use the following looks to show a code example and its results:
\begin{enverb}[same-line=0.6]
  \newcommand*\foo{This is an example.}
  \foo
\end{enverb}
And this is how a syntax summary or a syntax example looks like (this is more
abstract than an example and contains short meta-descriptions of inputs):
\begin{syntaxexample}
  \cs[no-index]{function}\marg{syntax}
\end{syntaxexample}
Inside such syntax summaries the following rules usually apply (and \meta{arg}
would be the meta description here):
\begin{description}[font=\small\ttfamily]
  \item[\marg{arg}]
    a mandatory argument is shown in braces
  \item[\meta{arg}]
    a mandatory argument that should be a single token is shown without
    additional parentheses/braces/brackets
  \item[\oarg{arg}]
    an optional argument is shown in brackets (and should be input with
    brackets)
  \item[\meta{*}]
    an optional star is shown like this
\end{description}
If other types of arguments are displayed the documentation will explain what
they mean in this particular place.

\addssec{Category Codes}
Supporting different category codes of a single character code makes the
programmer's life harder in \TeX, but there are valid reasons to make some
active, or letter. Because of this the packages in this bundle support different
category codes for specific syntax relevant characters (unless otherwise
documented). This doesn't mean that \expkv\ changes any category codes, only
that parsing is correct \emph{if} they are changed later (the codes listed
assume standard category codes of \hologo{plainTeX} and \LaTeXe\ apply while
\expkv\ is loaded). The supported tokens are:
\begin{description}
  \item[=] \tkn{=}{12} and \tkn{=}{13}
  \item[,] \tkn{,}{12} and \tkn{,}{13}
  \item[:] (for the \expnotation) \tkn{:}{11}, \tkn{:}{12}, and \tkn{:}{13}
  \item[*] (for starred macros) \tkn{*}{3}, \tkn{*}{4}, \tkn{*}{6}, \tkn{*}{7},
    \tkn{*}{8}, \tkn{*}{11}, \tkn{*}{12}, and \tkn{*}{13}
  \item[{[}] (for |\ekvoptarg|) only \tkn{[}{12}
  \item[{]}] (for |\ekvoptarg|) only \tkn{]}{12}
\end{description}

\addssec{Bugs}
Just like \pkg{keyval}, \expkv\ is bug free. But if you find \pmso{bugs}hidden
features\footnote{Thanks, David!} you can tell me about them either via mail
(see the first page) or directly on GitLab if you have an account there:
\url{https://gitlab.com/islandoftex/texmf/expkv-bundle}
