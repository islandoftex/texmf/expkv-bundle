\begin{filecontents}{opttest.sty}
\ProvidesPackage{opttest}[9999/12/12 test package]

\RequirePackage{expkv-opt}

\def\mybarG{Unused}
\def\mybarL{Unused}
\def\mybarF{Unused}
\def\myfooG{}
\def\myfooL{}
\def\myfooF{}
\ekvdef{testpkg-G}{foo}{\edef\myfooG{\unexpanded{#1}}}
\ekvdefNoVal{testpkg-G}{bar}{\def\mybarG{Used}}
\ekvdef{testpkg-L}{foo}{\edef\myfooL{\unexpanded{#1}}}
\ekvdefNoVal{testpkg-L}{bar}{\def\mybarL{Used}}
\ekvdef{testpkg-F}{foo}{\edef\myfooF{\unexpanded{#1}}}
\ekvdefNoVal{testpkg-F}{bar}{\def\mybarF{Used}}

\ekvoProcessGlobalOptions{testpkg-G}
\ekvoProcessLocalOptions{testpkg-L}
\ekvoProcessFutureOptions{testpkg-F}
\end{filecontents}

\begin{filecontents}{opttest.cls}
\ProvidesClass{opttest}[9999/12/12 test class]

\RequirePackage{expkv-opt,expkv-def}
\ekvdefinekeys{testcls}
  {
     store keyA = \clsA
    ,store keyB = \clsB
    ,store keyC = \clsC
    ,co: default keyA = c_novalue_tl
    ,v:  default keyB = c_novalue_tl
    ,co: default keyC = c_novalue_tl
    ,store a=\clsa
    ,store b=\clsb
    ,store c=\clsc
    ,meta keyD = {a={a#1a},b={b#1b},c={c#1c}}
  }

\ekvoProcessOptions{testcls}
\end{filecontents}

\input regression-test

\START
\OMIT
\RequirePackage[all]{expkv}
\makeatletter
\newcommand\Show[1]{\string#1 \meaning#1\NEWLINE}
\TIMO

\documentclass[foo=bar,pb: ar,\r: {keyA,keyB=\empty,keyC=abc},keyD=d]{opttest}

\usepackage[bar,co: foo=c_novalue_tl]{opttest}
\usepackage[foo=\empty]{opttest}

\TESTEXP{class-opts}
  {%
    \Show\clsA
    \Show\clsB
    \Show\clsC
    \Show\clsa
    \Show\clsb
    \Show\clsc
  }
\TESTEXP{package-opts}
  {%
    \Show\mybarG
    \Show\mybarL
    \Show\mybarF
    \Show\myfooG
    \Show\myfooL
    \Show\myfooF
  }
\TESTEXP{unused-opts}{\Show\@unusedoptionlist}
\END
